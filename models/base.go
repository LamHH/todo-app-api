package models

import (
	"fmt"
	"log"
	"os"
	"todo-app-api/services"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Server struct {
	DB *gorm.DB
}

var Model modelInterface = &Server{}

type modelInterface interface {
	Init() *gorm.DB

	GetUserByUsername(string) (*User, error)
	GetUserByAuth(*Auth) (*User, error)
	GetAuthByUserIdAndUUID(*services.AuthDetails) (*Auth, error)
	CreateAuth(uint64) (*Auth, error)
	CreateTodo(*gin.Context, *Todo) (*Todo, error)
	UpdateTodo(*gin.Context, *Todo) (*Todo, error)
	FindTodos(*gin.Context) ([]Todo, error)
	FindTodo(*gin.Context, uint64) (*Todo, error)
	DeleteTodo(*gin.Context, uint64) error
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func (s *Server) Init() (db *gorm.DB) {
	user := getEnv("POSTGRES_USER", "root")
	password := getEnv("POSTGRES_PASSWORD", "")
	host := getEnv("POSTGRES_HOST", "db")
	port := getEnv("POSTGRES_PORT", "5432")
	database := getEnv("POSTGRES_DB", "todo-app")

	dbinfo := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		user,
		password,
		host,
		port,
		database,
	)

	db, err := gorm.Open("postgres", dbinfo)
	s.DB = db
	if err != nil {
		log.Println("Failed to connect to database")
		panic(err)
	}
	log.Println("Database connected")
	db.AutoMigrate(&User{}, &Auth{}, &Todo{})
	// db.Create(&User{Username: "admin", Password: "123456"})
	return
}
