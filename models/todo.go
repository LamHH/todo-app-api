package models

import (
	"time"

	"github.com/gin-gonic/gin"
)

type Todo struct {
	ID        uint64    `gorm:"primary_key" json:"id"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	User      User      `json:"-"`
	UserID    uint64    `json:"userId"`
	Title     string    `gorm:"size:255;not null" json:"title"`
	Completed *bool     `sql:"DEFAULT:'FALSE'" json:"completed"`
}

func (s *Server) CreateTodo(c *gin.Context, todo *Todo) (*Todo, error) {
	currentUser := c.MustGet("currentUser").(*User)
	todo.UserID = currentUser.ID
	err := s.DB.Create(&todo).Error
	if err != nil {
		return nil, err
	}
	return todo, nil
}

func (s *Server) FindTodos(c *gin.Context) ([]Todo, error) {
	var todos []Todo
	currentUser := c.MustGet("currentUser").(*User)
	if err := s.DB.Model(&currentUser).Related(&todos).Error; err != nil {
		return nil, err
	}
	return todos, nil
}

func (s *Server) FindTodo(c *gin.Context, todoId uint64) (*Todo, error) {
	todo := Todo{ID: todoId}
	currentUser := c.MustGet("currentUser").(*User)
	if err := s.DB.Model(&currentUser).Related(&todo).Error; err != nil {
		return nil, err
	}
	return &todo, nil
}

func (s *Server) UpdateTodo(c *gin.Context, todoInput *Todo) (*Todo, error) {
	todo, err := s.FindTodo(c, todoInput.ID)
	if err != nil {
		return nil, err
	}
	if err := s.DB.Model(&todo).Update("completed", todoInput.Completed).Error; err != nil {
		return nil, err
	}
	return todo, nil
}

func (s *Server) DeleteTodo(c *gin.Context, todoId uint64) error {
	todo, err := s.FindTodo(c, todoId)
	if err != nil {
		return err
	}
	if err := s.DB.Delete(&todo).Error; err != nil {
		return err
	}
	return nil
}
