package models

import (
	"time"
)

type User struct {
	ID        uint64     `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
	Username  string     `gorm:"unique_index;not null" json:"username"`
	Password  string     `json:"-"`
	Todos     []Todo     `json:"-"`
}

func (s *Server) GetUserByUsername(username string) (*User, error) {
	var user User
	if err := s.DB.Where("username = ?", username).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (s *Server) GetUserByAuth(auth *Auth) (*User, error) {
	var user User
	if err := s.DB.Model(&auth).Related(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}
