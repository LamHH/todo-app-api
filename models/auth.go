package models

import (
	"todo-app-api/services"

	"github.com/google/uuid"
)

type Auth struct {
	ID       uint   `gorm:"primary_key;auto_increment" json:"-"`
	User     User   `json:"-"`
	UserID   uint64 `json:"-"`
	AuthUUID string `gorm:"size:255;not null;" json:"-"`
}

func (s *Server) CreateAuth(userId uint64) (*Auth, error) {
	au := &Auth{
		AuthUUID: uuid.New().String(),
		UserID:   userId,
	}
	if err := s.DB.Create(&au).Error; err != nil {
		return nil, err
	}
	return au, nil
}

func (s *Server) GetAuthByUserIdAndUUID(authDetails *services.AuthDetails) (*Auth, error) {
	var auth Auth
	if err := s.DB.Where("user_id = ? AND auth_uuid = ?", authDetails.UserId, authDetails.AuthUuid).First(&auth).Error; err != nil {
		return nil, err
	}
	return &auth, nil
}
