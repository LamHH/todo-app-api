package middlewares

import (
	"net/http"
	"todo-app-api/models"
	"todo-app-api/services"

	"github.com/gin-gonic/gin"
)

func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		claims, err := services.TokenValid(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, "You need to be authorized to access this route")
			c.Abort()
			return
		}
		tokenAuth, err := services.ExtractTokenAuth(claims)
		auth, err := models.Model.GetAuthByUserIdAndUUID(tokenAuth)

		if err != nil {
			c.JSON(http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		currentUser, err := models.Model.GetUserByAuth(auth)
		if err != nil {
			c.JSON(http.StatusNotFound, "User not found")
			c.Abort()
			return
		}
		c.Set("currentUser", currentUser)
		c.Next()
	}
}
