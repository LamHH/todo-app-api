package main

import (
	"log"
	"todo-app-api/controllers"
	"todo-app-api/middlewares"
	"todo-app-api/models"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	log.Println("Starting server..")
	r := gin.Default()

	// r.Use(cors.Default())
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"PUT", "PATCH", "DELETE"},
		AllowHeaders: []string{"Origin", "Authorization", "Content-Type"},
	}))

	models.Model.Init()

	r.POST("/login", controllers.Login)
	r.GET("/todos", middlewares.AuthRequired(), controllers.FindTodos)
	r.GET("/todos/:id", middlewares.AuthRequired(), controllers.FindTodo)
	r.POST("/todos", middlewares.AuthRequired(), controllers.CreateTodo)
	r.PATCH("/todos/:id", middlewares.AuthRequired(), controllers.UpdateTodo)
	r.DELETE("/todos/:id", middlewares.AuthRequired(), controllers.DeleteTodo)

	r.Run()
}
