package db

import (
	"fmt"
	"log"
	"os"
	"todo-app-api/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	db  *gorm.DB
	err error
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func Init() (db *gorm.DB) {
	user := getEnv("POSTGRES_USER", "root")
	password := getEnv("POSTGRES_PASSWORD", "")
	host := getEnv("POSTGRES_HOST", "db")
	port := getEnv("POSTGRES_PORT", "5432")
	database := getEnv("POSTGRES_DB", "todo-app")

	dbinfo := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		user,
		password,
		host,
		port,
		database,
	)

	db, err = gorm.Open("postgres", dbinfo)
	if err != nil {
		log.Println("Failed to connect to database")
		panic(err)
	}
	log.Println("Database connected")
	db.AutoMigrate(&models.User{}, &models.Auth{})
	return
}

func GetDB() *gorm.DB {
	return db
}

func CloseDB() {
	db.Close()
}
