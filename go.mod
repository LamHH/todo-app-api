module todo-app-api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.12
)
