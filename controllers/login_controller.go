package controllers

import (
	"todo-app-api/models"
	"todo-app-api/services"

	"net/http"

	"github.com/gin-gonic/gin"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func Login(c *gin.Context) {
	var loginInput login
	if err := c.ShouldBind(&loginInput); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"msg": "Invalid username/password"})
		return
	}

	user, err := models.Model.GetUserByUsername(loginInput.Username)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": "Invalid username/password"})
		return
	}

	if user.Password != loginInput.Password {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"msg": "Invalid username/password"})
		return
	}

	authData, err := models.Model.CreateAuth(user.ID)
	authDetails := services.AuthDetails{AuthUuid: authData.AuthUUID, UserId: authData.UserID}
	token, err := authDetails.CreateToken()

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"msg": "Please try to login later"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token, "user": user})
}
