package controllers

import (
	"net/http"
	"strconv"
	"todo-app-api/models"

	"github.com/gin-gonic/gin"
)

type createTodoInput struct {
	Title string `form:"title" json:"title" binding:"required"`
}

type updateTodoInput struct {
	Completed *bool `form:"completed" json:"completed" binding:"required"`
}

func FindTodos(c *gin.Context) {
	todos, err := models.Model.FindTodos(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusOK, todos)
}

func FindTodo(c *gin.Context) {
	todoId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	todo, err := models.Model.FindTodo(c, todoId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusOK, todo)
}

func CreateTodo(c *gin.Context) {
	var input createTodoInput
	if err := c.ShouldBind(&input); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"msg": err.Error()})
		return
	}
	todo, err := models.Model.CreateTodo(c, &models.Todo{Title: input.Title})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, todo)
}

func UpdateTodo(c *gin.Context) {
	var input updateTodoInput
	if err := c.ShouldBind(&input); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"msg": err.Error()})
		return
	}
	todoId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	todo, err := models.Model.UpdateTodo(c, &models.Todo{ID: todoId, Completed: input.Completed})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, todo)
}

func DeleteTodo(c *gin.Context) {
	todoId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	err = models.Model.DeleteTodo(c, todoId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusOK, "Success")
}
