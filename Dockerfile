FROM golang:1.13-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache bash git

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

ENTRYPOINT [ "go", "run", "main.go" ]